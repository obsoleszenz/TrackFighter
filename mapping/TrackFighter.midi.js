const FORCE_QUANTIZE = true;

const BUTTON_PRESSED = 127;
const BUTTON_RELEASED = 0;

const MODIFIER_PAGE_LOOPS      = 0;
const MODIFIER_PAGE_HOTCUE     = 1;
const MODIFIER_PAGE_HOTCUE_TWO = 2;
const MODIFIER_PAGE_EXTRAS     = 3;

const MODIFIER_BUTTON_ONE   = 0;
const MODIFIER_BUTTON_TWO   = 1;
const MODIFIER_BUTTON_THREE = 2;
const MODIFIER_BUTTON_FOUR  = 3;
const MODIFIER_BUTTON_FIVE  = 4;
const MODIFIER_BUTTON_SIX   = 5;
const MODIFIER_BUTTON_SEVEN = 6;
const MODIFIER_BUTTON_EIGHT = 7;


function DBG(str) {
  print("[TrackFighter] " + str);
}

function groupFromChannelIndex(channelIndex) {
  return '[Channel' + String(channelIndex) + ']';
}

function clearLEDS() {
  for (var i = 0; i < 5; i++) {
    midi.sendShortMsg(0x80, i, 0x1);
    midi.sendShortMsg(0x80, 0x32 + i, 0x1);
  }
}


var TrackFighter = {};
TrackFighter.selectedChannel = [false, false, false, false];
TrackFighter.activeChannel = -1;
TrackFighter.modifierPage = -1;
TrackFighter.shift = false;
TrackFighter.loadButtonPressed = false;
TrackFighter.buttonPressDuringChannelSelect = false;
TrackFighter.buttonPressDuringLoadTrack = false;

TrackFighter.init = function(id, debugging) {
  DBG("Hello from TrackFighter!");
  //clearLEDS();
  for (var i = 1; i < 5; i++) {
    var group = groupFromChannelIndex(i);
    resetDeck(group);
  }
  switchChannel(0);
  switchModifierPage(0);
}

function selectChannel(channelIndex) {
  DBG("Select channel " + channelIndex);
  midi.sendShortMsg(0x90, channelIndex, 0x1);
  TrackFighter.selectedChannel[channelIndex] = true;
  DBG(TrackFighter.selectedChannel);
}

function unselectChannel(channelIndex) {
  DBG("Unselect channel " + channelIndex);
  midi.sendShortMsg(0x80, channelIndex, 0x1);
  TrackFighter.selectedChannel[channelIndex] = false;
  DBG(TrackFighter.selectedChannel);
}

function switchChannel(channelIndex) {
  DBG("Switch channel from " + TrackFighter.activeChannel + " to " + channelIndex);
  if (TrackFighter.activeChannel !== -1) {
    midi.sendShortMsg(0x80, TrackFighter.activeChannel, 0x1);
  }
  TrackFighter.activeChannel = channelIndex;
  midi.sendShortMsg(0x90, TrackFighter.activeChannel, 0x1);
}

function countSelectedChannel() {
  var i = 0;
  forEachSelectedChannel(function() { i++ });
  return i;
}

function hasSelectedNonActiveChannel() {
  for (var i = 0; i < TrackFighter.selectedChannel.length; i++) {
    if (TrackFighter.selectedChannel[i] === true) return true;
  }
  return false;
}


function forEachSelectedChannel(cb) {
  var _hasSelectedNonActiveChannel = hasSelectedNonActiveChannel();
  if(_hasSelectedNonActiveChannel) {
    for (var i = 0; i < TrackFighter.selectedChannel.length; i++) {
      if (TrackFighter.selectedChannel[i] == true) {
        cb(i, groupFromChannelIndex(i + 1));
      }
    }
  } else {
    cb(TrackFighter.activeChannel, groupFromChannelIndex(TrackFighter.activeChannel + 1));
  }
}

function engineSetValueForSelectedChannels(key, value) {
  DBG("engineSetValueForSelectedChannels Hello" + key + " " + value);
  forEachSelectedChannel(function (i, group) {
    DBG("engineSetValueForSelectedChannels " + String(i) + " " + key + " " + value);
    engine.setValue(group, key, value)
  });
}

function registerButtonPressDuringChannelSelect() {
  if (hasSelectedNonActiveChannel()) {
    TrackFighter.buttonPressDuringChannelSelect = true;
  }
}

function unregisterButtonPressDuringChannelSelect() {
  if (!hasSelectedNonActiveChannel()) {
    TrackFighter.buttonPressDuringChannelSelect = false;
  }
}

const HIGH_FLAKE_DEBOUNCE_TRESHOLD = 30;
function Button(cb) {
  var last_high_flake = 0
  return function (channel, control, value, status, group) {
    if (value == BUTTON_PRESSED) {
      var now = Date.now()
      if ((now - last_high_flake) < HIGH_FLAKE_DEBOUNCE_TRESHOLD) return 
      last_high_flake = now
    }
    registerButtonPressDuringChannelSelect();
    registerButtonPressDuringLoadTrack(value);

    cb(channel, control, value, status, group);
  }
}

function registerButtonPressDuringLoadTrack(value) {
  if (value != BUTTON_PRESSED) return;

  if (TrackFighter.loadButtonPressed == true && TrackFighter.buttonPressDuringLoadTrack == false) {
    TrackFighter.buttonPressDuringLoadTrack = true
  }
}

function channelButton(buttonChannelIndex, value) {
  return function (channel, control, value, status, group) {
    if (value == BUTTON_PRESSED) {
      selectChannel(buttonChannelIndex);
    } else if (value == BUTTON_RELEASED) {
      unselectChannel(buttonChannelIndex);
      if (TrackFighter.shift) {
        if (TrackFighter.buttonPressDuringChannelSelect === false) {
          engine.setValue(groupFromChannelIndex(buttonChannelIndex + 1), 'CloneFromDeck', TrackFighter.activeChannel + 1);
          switchChannel(buttonChannelIndex);
          return;
        }
      }
      if (TrackFighter.buttonPressDuringChannelSelect === false) {
        switchChannel(buttonChannelIndex);
      }
      if (hasSelectedNonActiveChannel() === false) {
        unregisterButtonPressDuringChannelSelect();
      }
    }
  }
}

TrackFighter.channelOneButton = channelButton(0);
TrackFighter.channelTwoButton = channelButton(1);
TrackFighter.channelThreeButton = channelButton(2);
TrackFighter.channelFourButton = channelButton(3);



var libraryButtonTimers = {};
var LIBRARY_BUTTON_PRESSED_TIME_A = 400;
var LIBRARY_BUTTON_PRESSED_TIME_B = 50;
var MAGIC_TIMEOUT_PLAY = 175; // Time to wait before start playing a track after loading
function libraryButton(key) {
  return Button(function(channel, control, value, status, group) {
    DBG('libraryButton ' + key)
    const shiftPressed = TrackFighter.shift === true
    if (value == BUTTON_PRESSED && (shiftPressed || TrackFighter.loadButtonPressed)) {
      engine.setValue('[Library]', key, 1);
      onHold(
        function() {
          engine.setValue('[Library]', key, 1);
        },
        LIBRARY_BUTTON_PRESSED_TIME_A,
        LIBRARY_BUTTON_PRESSED_TIME_B,
        key
      );
    } else if (value === BUTTON_RELEASED) {
      stopOnHold(key);
      if (TrackFighter.loadButtonPressed) {
        forEachSelectedChannel(function(i, group) {
          engine.setValue(group, 'LoadSelectedTrack', 1);
          resetDeck(group);
          setTimeout(MAGIC_TIMEOUT_PLAY, function() {
            engine.setValue(group, 'play', 1)
          })
        })
      }
    }
    
  });
}

TrackFighter.previousTrackButton = libraryButton('MoveUp');

TrackFighter.cueButton = Button(function(channel, control, value, status, group) {
  const shiftPressed = TrackFighter.shift === true
  forEachSelectedChannel(function (i, group) {
    const isPlaying = engine.getValue(group, 'play')
    if (value == BUTTON_PRESSED) {
      if (!shiftPressed && !isPlaying) {
        engine.setValue(group, 'quantize', 0);
        engine.setValue(group, 'cue_default', 1);
        engine.setValue(group, 'beats_translate_curpos', 1);
      } else if (shiftPressed) {
        engine.setValue(group, 'cue_goto', 1);
      }
    } else if (value == BUTTON_RELEASED) {
      if (isPlaying && !shiftPressed) {
        if (engine.getValue(group, 'cue_default') == 1) {
          engine.setValue(group, 'cue_default', 0);
        }
      }
      engine.setValue(group, 'quantize', 1);
    }
  })
  /*
  } else if (value == BUTTON_RELEASED) {
    forEachSelectedChannel(function (i, group) {
      if (shiftPressed) {
        engine.setValue(group, 'quantize', 1);
        engine.setValue(group, 'playposition', 0);
      } else {
        if (engine.getValue(group, 'cue_default') === 1) {
          engine.setValue(group, 'cue_default', 0);
        }
        engine.setValue(group, 'quantize', 1);
      }
    });
  }
  */
});

TrackFighter.playButton = Button(function(channel, control, value, status, group) {
  if (value == BUTTON_PRESSED) {
    forEachSelectedChannel(function (i, group) {
      const isPlaying = engine.getValue(group, 'play')
      engine.setValue(group, 'play', !isPlaying)
      engine.setValue(group, 'cue_default', 0);
    });
  }
});

TrackFighter.nextTrackButton = libraryButton('MoveDown');

var lastTimeShiftButtonPressed = 0;
TrackFighter.shiftButton = function(channel, control, value, status, group) {
  if (value == BUTTON_PRESSED) {
    const now = Date.now();
    const delta = now - lastTimeShiftButtonPressed;
    DBG('shift press delta: ' + delta);
    //if (delta < 300) {
    //  DBG('shift Double click!');
    //  forEachSelectedChannel(function(i, group) {
    //    if (FORCE_QUANTIZE == true) {
    //      engine.setValue(group, 'quantize', 1);
    //    }
    //    engine.setValue(group, 'LoadSelectedTrack', 1);
    //    resetDeck(group);
    //  })
    //}
    lastTimeShiftButtonPressed = now;

    TrackFighter.shift = true;
    DBG('Shift pressed');
  } else {
    TrackFighter.shift = false;
    DBG('Shift released');
  }
};

const nudgeTimers = [null, null, null, null]

const NUDGE_INTERVALS_PER_REV = 1024;
const NUDGE_RPM = 33+1/3;
const NUDGE_ALPHA = 1.8 / 8;
const NUDGE_BETA = NUDGE_ALPHA / 32;
const NUDGE_RAMP = true;

const NUDGE_RATE_FORWARD = 5;
const NUDGE_RATE_FORWARD_LITTLE = 2;
const NUDGE_RATE_BACKWARD = - NUDGE_RATE_FORWARD;
const NUDGE_RATE_BACKWARD_LITTLE = - NUDGE_RATE_FORWARD_LITTLE;

function stopNudge(i, deck, group) {
  if (nudgeTimers[i] === null) return

  engine.scratchDisable(deck)
  engine.stopTimer(nudgeTimers[i])
  nudgeTimers[i] = null
  DBG("stopNudge " + i + " " + deck);
  engine.setValue(group, 'beats_translate_curpos', 1);
}
function startNudge(i, deck, interval) {
  if (typeof i === 'undefined') {
    DBG('startNudge: i is undefined??? i=' + i)
    return
  }

  if (nudgeTimers[i] !== null) stopNudge(i, deck)
  engine.scratchEnable(deck, NUDGE_INTERVALS_PER_REV, NUDGE_RPM, NUDGE_ALPHA, NUDGE_BETA, NUDGE_RAMP);

  nudgeTimers[i] = engine.beginTimer(20, function() {
    DBG('scratchTick!')
    engine.scratchTick(deck, interval)
  })
}

const ON_HOLD_PITCH_TIME_A = 130;
const ON_HOLD_PITCH_TIME_B = 60;

function rateTempButton(nudge_interval, key_rate, key_rate_perm) {
  return Button(function(_channel, control, value, status, group) {
    if (value == BUTTON_PRESSED) {
      if (TrackFighter.shift) {
        const pitch = function() {
          if (key_rate_perm === false) return;
          forEachSelectedChannel(function (i, group) {
            engine.setValue(group, key_rate_perm, 1);
          })
        }
        pitch();
        onHold(pitch, ON_HOLD_PITCH_TIME_A, ON_HOLD_PITCH_TIME_B, key_rate_perm);
      } else {
        forEachSelectedChannel(function (i, group) {
          DBG('hallo2 ' + engine.getValue(group, 'play') + ' ' + i)
          if (engine.getValue(group, 'play') === 0) {
            DBG('startNudge ' + group + ' ' + i + ' ' + nudge_interval);
            startNudge(i, script.deckFromGroup(group), nudge_interval)
            return;
          }
          engine.setValue(group, key_rate, 1);
        });
      }
    } else if (value === BUTTON_RELEASED) {
      stopOnHold(key_rate_perm);
      forEachSelectedChannel(function (i, group) {
        if (engine.getValue(group, 'play') === 0) {
          DBG('stopNudge ' + group);
          stopNudge(i, script.deckFromGroup(group), group)
          return;
        }

        if (engine.getValue(group, key_rate) === 1) {
          engine.setValue(group, key_rate, 0);
        }

        if (engine.getValue(group, 'play') !== 0) {
          engine.setValue(group, 'beats_translate_match_alignment', 1);
        }
      });
    }
  })
}

TrackFighter.slowerButton = rateTempButton(    NUDGE_RATE_BACKWARD,        'rate_temp_down',       'rate_perm_down'      );
TrackFighter.tinySlowerButton = rateTempButton(NUDGE_RATE_BACKWARD_LITTLE, 'rate_temp_down_small', 'rate_perm_down_small');
TrackFighter.tinyFasterButton = rateTempButton(NUDGE_RATE_FORWARD_LITTLE,  'rate_temp_up_small',   'rate_perm_up_small'  );
TrackFighter.fasterButton = rateTempButton(    NUDGE_RATE_FORWARD,         'rate_temp_up',         'rate_perm_up'        );


function ModifierButton(modifier_button) {
  const hotcue_id = modifier_button + 1
  return Button(function(_channel, _control, value, _status, _group) {
    const modifierPage = TrackFighter.modifierPage

    if (modifierPage === MODIFIER_PAGE_LOOPS) {
      if (modifier_button < 4) {
        ModifierButtonBeatJump(modifier_button, value);
      } else {
        ModifierButtonBeatLoop(modifier_button, value);
      }
    } else if (modifierPage === MODIFIER_PAGE_HOTCUE) {
      if (TrackFighter.shift) {
        value == BUTTON_PRESSED && engineSetValueForSelectedChannels('hotcue_' + hotcue_id + '_clear', 1);
      } else {
        forEachSelectedChannel(function(_i, group) {
          if (value == BUTTON_PRESSED) {
            engine.setValue(group, 'hotcue_' + hotcue_id + '_activate', 1);
          } else {
            engine.setValue(group, 'hotcue_' + hotcue_id + '_activate', 0);
          }
        });
      }
    } else if (modifierPage === MODIFIER_PAGE_HOTCUE_TWO) {
    } else if (modifierPage === MODIFIER_PAGE_EXTRAS) {
    }
  })
}

const BEATJUMP_SIZES = [32, 1, 1, 32];
const BEATJUMP_SIZES_SHIFT = [8, 4, 4, 8];

function ModifierButtonBeatJump(modifier_button, value) {
  const direction = modifier_button < 2 ? 'backward' : 'forward';
  const beatjump_size = TrackFighter.shift === false ?
    BEATJUMP_SIZES[modifier_button] :
    BEATJUMP_SIZES_SHIFT[modifier_button];

  DBG('ModifierButtonBeatJump [' + modifier_button + '] beatjump_size: ' + beatjump_size + ' direction: ' + direction); 

  value == BUTTON_PRESSED &&
    engineSetValueForSelectedChannels('beatjump_' + beatjump_size + '_' + direction, 1);
}

function ModifierButtonBeatLoop(modifier_button, value) { 
  var size = [1, 4, 8, 16][modifier_button % 4]; 
  if (TrackFighter.shift) {
    if (size == 1 || size == 16) {
      value == BUTTON_PRESSED && forEachSelectedChannel(function (_i, group) {
        loop_size = size === 1 ?
          Math.max(Math.floor(engine.getValue(group, 'beatloop_size') / 2), 1) :
          Math.floor(engine.getValue(group, 'beatloop_size') * 2);
        engine.setValue(group, 'beatloop_size', loop_size);
      })
    } else {
      value == BUTTON_PRESSED && forEachSelectedChannel(function (_i, group) {
        if (size == 4 && engine.getValue(group, 'loop_enabled') == true) engine.setValue(group, 'reloop_toggle', 1);
        if (size == 8 && engine.getValue(group, 'loop_enabled') == false) engine.setValue(group, 'reloop_toggle', 1);
      })
    }
  } else {
    value == BUTTON_PRESSED && forEachSelectedChannel(function (_i, group) {
      var activate = engine.getValue(group, 'beatloop_size') === size;
      engine.setValue(group, 'beatloop_size', size);
      engine.setValue(group, 'beatloop_activate', 1);
    })
  }
}

TrackFighter.modifierOneButton   = ModifierButton(MODIFIER_BUTTON_ONE);
TrackFighter.modifierTwoButton   = ModifierButton(MODIFIER_BUTTON_TWO);
TrackFighter.modifierThreeButton = ModifierButton(MODIFIER_BUTTON_THREE);
TrackFighter.modifierFourButton  = ModifierButton(MODIFIER_BUTTON_FOUR);
TrackFighter.modifierFiveButton  = ModifierButton(MODIFIER_BUTTON_FIVE);
TrackFighter.modifierSixButton   = ModifierButton(MODIFIER_BUTTON_SIX);
TrackFighter.modifierSevenButton = ModifierButton(MODIFIER_BUTTON_SEVEN);
TrackFighter.modifierEightButton = ModifierButton(MODIFIER_BUTTON_EIGHT);

function switchModifierPage(pageIndex) {
  var previousPage = TrackFighter.modifierPage;
  midi.sendShortMsg(0x80, 0x32 + previousPage, 0x1);
  TrackFighter.modifierPage = pageIndex;
  midi.sendShortMsg(0x90, 0x32 + pageIndex, 0x1);
  DBG("Switched modifier page from " + String(previousPage) + " to " + String(pageIndex));
}

function switchModifierPageButton(pageIndex) {
  return Button(function(_channel, _control, value, _status, _group) {
    value == BUTTON_PRESSED && switchModifierPage(pageIndex);
    value == BUTTON_RELEASED && switchModifierPage(MODIFIER_PAGE_LOOPS);
  });
}

TrackFighter.modifierPageOneButton   = switchModifierPageButton(MODIFIER_PAGE_LOOPS)
TrackFighter.modifierPageTwoButton   = switchModifierPageButton(MODIFIER_PAGE_HOTCUE)
TrackFighter.modifierPageThreeButton = switchModifierPageButton(MODIFIER_PAGE_HOTCUE_TWO)

// Load track button
TrackFighter.modifierPageFourButton  = Button(function(_channel, _control, value, _status, _group) {
  TrackFighter.loadButtonPressed = value === BUTTON_PRESSED
  if (value != BUTTON_RELEASED) return
  forEachSelectedChannel(function(i, group) {
    if (FORCE_QUANTIZE == true) {
      engine.setValue(group, 'quantize', 1);
    }
    if (TrackFighter.buttonPressDuringLoadTrack === false) {
      engine.setValue(group, 'LoadSelectedTrack', 1);
      resetDeck(group);
    } else {
      TrackFighter.buttonPressDuringLoadTrack = false
    }
  })
})

TrackFighter.shutdown = function() {
  DBG("Goodbye from TrackFighter!");
  clearLEDS();
}

// H E L P E R S
var onHoldTimers = {}
function onHold(cb, timeA, timeB, key) {
  if (!onHoldTimers[key]) {
    onHoldTimers[key] = engine.beginTimer(timeA, function() {
      cb();
      engine.stopTimer(onHoldTimers[key]);
      onHoldTimers[key] = engine.beginTimer(timeB, function() {
        cb();
      })
    }); 
  }

}
function stopOnHold(key) {
  if (!onHoldTimers[key]) return;
  engine.stopTimer(onHoldTimers[key]);
  onHoldTimers[key] = null;
}


function resetDeck(group) {
  if (FORCE_QUANTIZE === true) {
    engine.setValue(group, 'quantize', 1);
    engine.setValue(group, 'sync_enabled', 1);
  } else {
    engine.setValue(group, 'quantize', 0);
    engine.setValue(group, 'sync_enabled', 1);

  }
}

function setTimeout(timer, cb) {
  var timer = engine.beginTimer(timer, function() {
    cb();
    engine.stopTimer(timer);
  });
}