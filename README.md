

# TrackFighter - Functional DJ controller built from trash 

## Description

A dj controller meant for fast 4 deck mixing with synced tracks. It's built from things found in the street (buttons/leds/wood) and a arduino uno. This repo contains some random sketches and the firmware code. Probably not possible to rebuild an exact copy of this one, but maybe getting inspired or looking up code/ideas/mappings.

## Pictures

<img src="https://codeberg.org/obsoleszenz/dj-controller-trackfighter/raw/branch/main/docs/trackfighter-new-case.jpg" alt="Picture of the finished controller" height="300px"/>

<img src="https://codeberg.org/obsoleszenz/dj-controller-trackfighter/raw/branch/main/docs/trackfighter-new-case-open.jpg" alt="Picture of the finished controller and open case" height="300px"/>

<img src="https://codeberg.org/obsoleszenz/dj-controller-trackfighter/raw/branch/main/docs/trackfighter-new-case-perfboard-arduino.jpg" alt="Picture of the perfboard and arduino" height="300px"/>

## Mapping

<img src="https://codeberg.org/obsoleszenz/dj-controller-trackfighter/raw/branch/main/docs/mapping-manual.png" alt="Picture of the button mapping" height="700px"/>

